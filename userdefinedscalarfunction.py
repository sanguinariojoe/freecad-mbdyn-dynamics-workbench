# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
#edge.discretize

import FreeCAD
import numpy as np

class userDefinedScalarFunction:
    def __init__(self, obj, label, steps): 
        
        obj.addExtension("App::GroupExtensionPython")
                
        obj.addProperty("App::PropertyString","label","Scalar function","label",1).label = label
        obj.addProperty("App::PropertyString","function type","Scalar function","function type",1).function_type = "user-defined"
        obj.addProperty("App::PropertyString","independent variable","Scalar function","independent variable").independent_variable = "Time"
        
        obj.addProperty("App::PropertyFloat","initial value","Domain","initial value").initial_value = FreeCAD.ActiveDocument.getObject("MBDyn").initial_time.Value
        obj.addProperty("App::PropertyFloat","final value","Domain","final value").final_value = FreeCAD.ActiveDocument.getObject("MBDyn").final_time.Value
        
        #function type
        obj.addProperty("App::PropertyEnumeration","type","Scalar function","type")
        obj.type=['cubicspline: cubic natural spline interpolation between the set of points (insert the points in the text file within this object)',
                  'multilinear: multilinear interpolation between the set of points (insert the points in the text file within this object)',
                  'chebychev: Chebychev interpolation between the set of points (insert the points in the text file within this object)']
  
        obj.addProperty("App::PropertyEnumeration","extrapolation","Scalar function","extrapolation")
        obj.extrapolation=['do not extrapolate', 'extrapolate']
                
        obj.Proxy = self

        obj = FreeCAD.ActiveDocument.addObject("App::TextDocument", "points_"+ label)   
        obj.Label = 'points: '+ label
        
        #Add an initial scalar function. This is assumed to be a function of time.
        InitialTime = FreeCAD.ActiveDocument.getObject("MBDyn").initial_time.Value
        FinalTime = FreeCAD.ActiveDocument.getObject("MBDyn").final_time.Value
        time = np.linspace(InitialTime, FinalTime, num=steps)    
        
        for i in time:    
            obj.Text = obj.Text + str(i)+ ',   0.0, \n'
            
        obj.Text = obj.Text[:-3]#Remove the last comma
                               
        FreeCAD.ActiveDocument.recompute()

    def execute(self, fp):
        obj = FreeCAD.ActiveDocument.getObjectsByLabel("points: "+fp.label)[0].Text.replace(" ","").replace("\n","").split(",")
        points = FreeCAD.ActiveDocument.getObjectsByLabel("points: "+fp.label)[0]
        Yvalues = obj[1::2]        
        steps = len(Yvalues)
        
        InitialValue = fp.initial_value
        FinalValue = fp.final_value
        
        time = np.linspace(InitialValue, FinalValue, num=steps) 
        
        
        points.Text = ""
        aux = 0
        for i in Yvalues:    
            points.Text = points.Text + '        ' + str(time[aux])+ ',   '+str(i)+', \n'
            aux = aux + 1    
        
        points.Text = points.Text[:-3]#Remove the last comma
        
        FreeCAD.Console.PrintMessage("SCALAR FUNCTION: " +fp.label+" successful recomputation...\n") 
        

            
            
                   
