# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This class implements a scripted object for a revolute pin joint. This joint constrains a node only allowing rotation about one axis. 
The basic syntax for the statement that defines a revolute pin is as follows:

The syntax is: 

joint: <label>, 
       revolute pin, 
       <node>, 
       <relative offset>, 
       hinge, 
       <relative orientation matrix>,
       <absolute pin position>, 
       hinge, 
       <absolute pin orientation matrix>;

Where: 

    <label>: is an integer number to identify the joint, eg: 1,2,3... 
    <node>: is the label of the structural node to which the joint is attached, eg: 1,2,3... 
    <relative offset>: is the possition of the joint relative to it's structural node.     
    <relative orientation matrix>:
    <absolute pin position>: is the pin possition relative to the absolute reference frame.
 
The constructor receives 7 parameters:
    
    label: it is a string of a positive integer number "1", "2", etc... that identifies the joint
    node: it is the node to which the joit is attached
    referenceObject1 and referenceObject2: the CAD shapes that contain the reference geometries "reference1" and "reference2", respectively
    reference1 and reference2: two reference geometries whose centres of mass define the joint´s axis and position 
    
    
'''

import FreeCAD#, FreeCADGui
import Draft
from sympy import Point3D, Line3D

class Revolutepin:
    def __init__(self, obj, label, node, referenceObject1, referenceObject2, reference1, reference2):
                
        obj.addExtension("App::GroupExtensionPython") 
        
        #Add properties:
        obj.addProperty("App::PropertyString","label","Revolute pin","label",1).label = label
        obj.addProperty("App::PropertyString","node_label","Revolute pin","node_label",1).node_label = node.label
        obj.addProperty("App::PropertyString","joint","Revolute pin","joint",1).joint = 'revolute pin'
        obj.addProperty("App::PropertyString","plugin_variables","Revolute pin","plugin_variables",1).plugin_variables = "none"
        
        #pin possition relative to it's node:
        obj.addProperty("App::PropertyDistance","relative_offset_X","Relative offset","relative_offset_X",1)
        obj.addProperty("App::PropertyDistance","relative_offset_Y","Relative offset","relative_offset_Y",1)
        obj.addProperty("App::PropertyDistance","relative_offset_Z","Relative offset","relative_offset_Z",1)
        
        #Absolute pin position:                
        obj.addProperty("App::PropertyDistance","absolute_pin_position_X","Absolute pin position","absolute_pin_position_X",1)
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Y","Absolute pin position","absolute_pin_position_Y",1)
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Z","Absolute pin position","absolute_pin_position_Z",1)  
        
        #The references define the position and orientation of the joint:                
        #obj.addProperty("App::PropertyLinkSub","reference_1","References","reference_1")
        obj.addProperty("App::PropertyLinkSub","reference_1","Reference 1","reference_1")
        obj.addProperty("App::PropertyEnumeration","attachment_mode_1","Reference 1","attachment mode 1")
        obj.attachment_mode_1 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']
        
        obj.addProperty("App::PropertyLinkSub","reference_2","Reference 2","reference_2")        
        obj.addProperty("App::PropertyEnumeration","attachment_mode_2","Reference 2","attachment mode 2")
        obj.attachment_mode_2 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']

        if referenceObject1 == referenceObject2:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[1])            
        
        else:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[0]) 
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']        
        
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0
        
        obj.Proxy = self
                        
        #Add joint´s rotation axis. This axis determines the "absolute_pin_orientation_matrix" and the jont´s position:                    
        p1 = obj.reference_1[0].Shape.getElement(obj.reference_1[1][0]).CenterOfMass
        p2 = obj.reference_2[0].Shape.getElement(obj.reference_2[1][0]).CenterOfMass
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        
        #Create the rotation axis:
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label          
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)
        l.ViewObject.DrawStyle = u"Dashed"   
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00
        l.ViewObject.EndArrow = True
        l.ViewObject.ArrowType = u"Arrow"
        l.ViewObject.ArrowSize = str(Llength/100)#+' mm'
                     
        #Add the vector to visualize reaction forces        
        p1 = FreeCAD.Vector((p1.x+p2.x)/2 , (p1.y+p2.y)/2, (p1.z+p2.z)/2)
        p2 = FreeCAD.Vector( p1.x+Llength.Value, p1.y+Llength.Value, p1.z+Llength.Value)  
        
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label         

        #absolute orientation:            
        obj.addProperty("App::PropertyString","absolute_pin_orientation_matrix","Orientation","absolute_pin_orientation_matrix",1)#.absolute_pin_orientation_matrix #= """3, " + str(l1.direction[0]) + ", "+ str(l1.direction[1]) + ", " + str(l1.direction[2]) + ", " +"2, guess"                     
        
        #relative orientation:   
        obj.addProperty("App::PropertyString","relative_orientation_matrix","Orientation","relative_orientation_matrix",1)#.relative_orientation_matrix #= """3, " + str(l1.direction[0]) +", "+ str(l1.direction[1]) + ", " + str(l1.direction[2]) + ", " +"2, guess"                     


    def execute(self, fp):   
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s rotation axis (Z)
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        
        
        if fp.attachment_mode_1 == 'geometry´s center of mass':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).CenterOfGravity
            
        if fp.attachment_mode_2 == 'geometry´s center of mass':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).CenterOfGravity
        
        if fp.attachment_mode_1 == 'body´s center of mass':
            p1 = fp.reference_1[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_2 == 'body´s center of mass':    
            p2 = fp.reference_2[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_1 == 'arc´s center':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).Curve.Center
            
        if fp.attachment_mode_2 == 'arc´s center':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).Curve.Center


        ZZ.Start = p1
        ZZ.End = p2
        
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        l1 = Line3D(p1, p2)#Line that defines the joint
        
        magnitude = (l1.direction[0]**2+l1.direction[1]**2+l1.direction[2]**2)**0.5#Calculate the vector´s magnitude
        
        #generate the orientation matrix:
        fp.absolute_pin_orientation_matrix = "3, "+ str(l1.direction[0]/magnitude) +", "+ str(l1.direction[1]/magnitude) + ", " + str(l1.direction[2]/magnitude) + ", " +"2, guess"                
                        
        ##############################################################################Recalculate the joint's position:
        #get and update the absolute pin position:
        x = FreeCAD.Units.Quantity((ZZ.Start[0] + ZZ.End[0])/2,FreeCAD.Units.Unit('mm')) 
        y = FreeCAD.Units.Quantity((ZZ.Start[1] + ZZ.End[1])/2,FreeCAD.Units.Unit('mm')) 
        z = FreeCAD.Units.Quantity((ZZ.Start[2] + ZZ.End[2])/2,FreeCAD.Units.Unit('mm')) 
        
        JF.Start =  FreeCAD.Vector(x, y, z)
        JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength)    
        
        fp.absolute_pin_position_X = x
        fp.absolute_pin_position_Y = y
        fp.absolute_pin_position_Z = z
        
        #get the node:
        node = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_label)[0]
        
        #Re-calculate the joint possition relative to it's node (relative offset)        
        x1 = x - node.absolute_position_X
        y1 = y - node.absolute_position_Y
        z1 = z - node.absolute_position_Z

        #Update the offset:
        fp.relative_offset_X = x1
        fp.relative_offset_Y = y1
        fp.relative_offset_Z = z1           
        
        ##############################################################################Calculate relative orientation:            
        zz = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]
        p11, p22 = Point3D(zz.Start[0], zz.Start[1], zz.Start[2]), Point3D(zz.End[0], zz.End[1], zz.End[2]) 
        l11 = Line3D(p11, p22)
        zzmagnitude1 = (l11.direction[0]**2+l11.direction[1]**2+l11.direction[2]**2)**0.5
                
        #generate the relative orientation matrix:
        #fp.relative_orientation_matrix = "3, "+ str(round(l1.direction[0]/zzmagnitude,precission)) +", "+ str(round(l1.direction[1]/zzmagnitude,precission)) + ", " + str(round(l1.direction[2]/zzmagnitude,precission)) + ", " +"2, "+ str(round(l2.direction[0]/yymagnitude,precission)) +", "+ str(round(l2.direction[1]/yymagnitude,precission)) + ", " + str(round(l2.direction[2]/yymagnitude,precission))                
        fp.relative_orientation_matrix = "3, "+ str(l11.direction[0]/zzmagnitude1) +", "+ str(l11.direction[1]/zzmagnitude1) + ", " + str(l11.direction[2]/zzmagnitude1) + ", " +"2, guess"#+ str(round(l2.direction[0]/yymagnitude,precission)) +", "+ str(round(l2.direction[1]/yymagnitude,precission)) + ", " + str(round(l2.direction[2]/yymagnitude,precission))                
        
        FreeCAD.Console.PrintMessage("REVOLUTE PIN JOINT: " +fp.label+" successful recomputation...\n")


