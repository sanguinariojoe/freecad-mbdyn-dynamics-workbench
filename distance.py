# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
8.12.25 In line

This joint forces a point relative to the second node to move along a line attached to the first node.

<joint_type> ::= in line
    <joint_arglist> ::=
        <node_1_label> ,
            [ position , (Vec3) <relative_line_position> , ]
            [ orientation , (OrientationMatrix) <relative_orientation> , ]
        <node_2_label>
            [ , offset , (Vec3) <relative_offset> ]
            
A point, optionally offset by relative_offset from the position of node node_2_label, slides along a
line that passes through a point that is rigidly offset by relative_line_position from the position of
node_1_label, and is directed as direction 3 of relative_orientation. The joint is shown in Figure 8.9
where the origin and orientation of node_1 are given by axis 1′, 2′, 3′ and relative_line_position and
relative_orientation are the transformations that yield axis 1′
0, 2′
0, 3′
0 when applied to its coordinate
axis. The origin and orientation of node_2 are given by axis 1, 2, 3 and axis 10, 20, 30 is the result of
applying the relative_offset translation to its coordinate axis.

'''

import FreeCAD
import Draft
from sympy import Point3D, Line3D

class Distance:
    def __init__(self, obj, label, node1, node2, referenceObject1, referenceObject2, reference1, reference2):        

        obj.addExtension("App::GroupExtensionPython")  
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","Distance joint","label",1).label = label
        obj.addProperty("App::PropertyString","joint","Distance joint","joint",1).joint = 'distance'
        obj.addProperty("App::PropertyString","node 1","Distance joint","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","Distance joint","node 2",1).node_2 = node2.label
        
        #The relative offset is initially set to (0,0,0), this is, node 2 is at the line, without offset
        obj.addProperty("App::PropertyDistance","relative offset X_1","Line offset relative to node 2","relative offset X_1",1).relative_offset_X_1 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset Y_1","Line offset relative to node 2","relative offset Y_1",1).relative_offset_Y_1 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset Z_1","Line offset relative to node 2","relative offset Z_1",1).relative_offset_Z_1 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))

        obj.addProperty("App::PropertyDistance","relative offset X_2","Line offset relative to node 2","relative offset X_2",1).relative_offset_X_2 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset Y_2","Line offset relative to node 2","relative offset Y_2",1).relative_offset_Y_2 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        obj.addProperty("App::PropertyDistance","relative offset Z_2","Line offset relative to node 2","relative offset Z_2",1).relative_offset_Z_2 = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))        
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']  
        
        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = '2'
        
        obj.addProperty("App::PropertyFloat","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = 1.0

        
        #The references define the position and orientation of the joint:                
        obj.addProperty("App::PropertyLinkSub","reference_1","Reference 1","reference_1")
        obj.addProperty("App::PropertyEnumeration","attachment_mode_1","Reference 1","attachment mode 1")
        obj.attachment_mode_1 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']
        
        obj.addProperty("App::PropertyLinkSub","reference_2","Reference 2","reference_2")        
        obj.addProperty("App::PropertyEnumeration","attachment_mode_2","Reference 2","attachment mode 2")
        obj.attachment_mode_2 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']      

        if referenceObject1 == referenceObject2:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[1])            
        
        else:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[0]) 

        obj.Proxy = self
                
        #Add joint´s rotation axis. This axis determines the "absolute_pin_orientation_matrix" and the jont´s position:                    
        p1 = obj.reference_1[0].Shape.getElement(obj.reference_1[1][0]).CenterOfMass
        p2 = obj.reference_2[0].Shape.getElement(obj.reference_2[1][0]).CenterOfMass
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))       
                     
        #Add the vector to visualize reaction forces        
        p1 = FreeCAD.Vector((p1.x+p2.x)/2 , (p1.y+p2.y)/2, (p1.z+p2.z)/2)
        p2 = FreeCAD.Vector( p1.x+Llength.Value, p1.y+Llength.Value, p1.z+Llength.Value)  
        
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        d.Label = "jf: "+ label                                        
        
    def execute(self, fp):
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s Z line
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        
        if fp.attachment_mode_1 == 'geometry´s center of mass':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).CenterOfGravity
            
        if fp.attachment_mode_2 == 'geometry´s center of mass':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).CenterOfGravity
        
        if fp.attachment_mode_1 == 'body´s center of mass':
            p1 = fp.reference_1[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_2 == 'body´s center of mass':    
            p2 = fp.reference_2[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_1 == 'arc´s center':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).Curve.Center
            
        if fp.attachment_mode_2 == 'arc´s center':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).Curve.Center

        ZZ.Start = p1
        ZZ.End = p2
        
        #Two 3D points that define the joint´s line:
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        #A line that defines the joint:
        l1 = Line3D(p1, p2)#Line that defines the joint
        #generate the orientation matrix:   
        zzmagnitude = (l1.direction[0]**2 + l1.direction[1]**2 + l1.direction[2]**2)**0.5 
        fp.relative_orientation = "3, "+ str(l1.direction[0]/zzmagnitude) +", "+ str(l1.direction[1]/zzmagnitude) +", "+ str(l1.direction[2]/zzmagnitude) + ", " +"2, guess" #"3, "+ str(round(l1.direction[0],precission)) +", "+ str(round(l1.direction[1],precission)) + ", " + str(round(l1.direction[2],precission)) + ", " +"2, guess"                               
        
        #Get the joint´s nodes:
        node1 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_1)[0]
        node2 = FreeCAD.ActiveDocument.getObjectsByLabel("structural: "+fp.node_2)[0]
        
        #Get the position of the nodes:
        p_node1 = Point3D(node1.absolute_position_X.Value, node1.absolute_position_Y.Value, node1.absolute_position_Z.Value)    
        p_node2 = Point3D(node2.absolute_position_X.Value, node2.absolute_position_Y.Value, node2.absolute_position_Z.Value)
        #Calculate a perpendicular line to obtain the line offset relative to node2:
        l2 = l1.perpendicular_line(p_node2)
        #Calculate a perpendicular line to obtain the line position relative to node1:
        l3 = l1.perpendicular_line(p_node1)
        
        #Update the relative offset:
        if l1.distance(p_node2) == 0:
            fp.relative_offset_X = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
            fp.relative_offset_Y = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
            fp.relative_offset_Z = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm'))
        else:
            relative_offset =  FreeCAD.Vector(float(l2.p2[0]),float(l2.p2[1]),float(l2.p2[2])) - FreeCAD.Vector(float(l2.p1[0]),float(l2.p1[1]),float(l2.p1[2]))         
            fp.relative_offset_X = FreeCAD.Units.Quantity(relative_offset[0],FreeCAD.Units.Unit('mm'))
            fp.relative_offset_Y = FreeCAD.Units.Quantity(relative_offset[1],FreeCAD.Units.Unit('mm'))
            fp.relative_offset_Z = FreeCAD.Units.Quantity(relative_offset[2],FreeCAD.Units.Unit('mm'))
        
        #Update the relative line position:
        
        if l1.distance(p_node1) == 0:
            fp.relative_line_position_X = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm')) 
            fp.relative_line_position_Y = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm')) 
            fp.relative_line_position_Z = FreeCAD.Units.Quantity(0.0,FreeCAD.Units.Unit('mm')) 
        else:        
            relative_line_position =  FreeCAD.Vector(float(l3.p2[0]),float(l3.p2[1]),float(l3.p2[2])) - FreeCAD.Vector(float(l3.p1[0]),float(l3.p1[1]),float(l3.p1[2]))         
            fp.relative_line_position_X = FreeCAD.Units.Quantity(relative_line_position[0],FreeCAD.Units.Unit('mm')) 
            fp.relative_line_position_Y = FreeCAD.Units.Quantity(relative_line_position[1],FreeCAD.Units.Unit('mm')) 
            fp.relative_line_position_Z = FreeCAD.Units.Quantity(relative_line_position[2],FreeCAD.Units.Unit('mm')) 
        
        #get and update the pin position:
        x = FreeCAD.Units.Quantity((ZZ.Start[0] + ZZ.End[0])/2,FreeCAD.Units.Unit('mm')) 
        y = FreeCAD.Units.Quantity((ZZ.Start[1] + ZZ.End[1])/2,FreeCAD.Units.Unit('mm')) 
        z = FreeCAD.Units.Quantity((ZZ.Start[2] + ZZ.End[2])/2,FreeCAD.Units.Unit('mm')) 
        
        fp.absolute_pin_position_X = x
        fp.absolute_pin_position_Y = y
        fp.absolute_pin_position_Z = z
        
        #Move the force arrow:            
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        JF.Start =  FreeCAD.Vector(x, y, z)
        JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength) 
        
        FreeCAD.Console.PrintMessage("IN-LINE JOINT: " +fp.label+" successful recomputation...\n")
        
        
        
'''

        
        p3 = Point3D(node2.absolute_position_X.Value, node2.absolute_position_Y.Value, node2.absolute_position_Z.Value)
         
        if (node1.absolute_position_X.Value == node2.absolute_position_X.Value and node1.absolute_position_Y.Value == node2.absolute_position_Y.Value and node1.absolute_position_Z.Value == node2.absolute_position_Z.Value) or (p3 in l1):
            
            fp.relative_offset_X = "0.0 m"
            fp.relative_offset_Y = "0.0 m"
            fp.relative_offset_Z = "0.0 m"
            
        else:    
        

        

            
            JF.Start =  FreeCAD.Vector(x, y, z)
            JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength) 
            

        
                                          

'''                                  
                                        