# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################
'''
This class implements an in-plane joint between two nodes. It receives an integer number which is used to label the joint, and two nodes. The second node is then constrained to a line fixed to the first node.

An in-line joint forces a point relative to the second node to move along a line attached to the first node.
A point, optionally offset by "relative offset" from the position of "node 2", slides along a
line that passes through a point that is rigidly offset by "relative_line_position" from the position of
"node 1", and is directed as direction 3 (the Z axis) of "relative orientation". 

The joint is defined as:

     joint: 2, #joint label
            in plane,
            2, # first asociated node
                position, 0., 0., 0., #relative plane position
                0., 0., 1., #relative normal direction
            1, # second asociated node
                offset, 0.0, 0.0, 0.0; # relatove offset

'''

#from FreeCAD.Units.Units import FreeCAD.Units.Unit,FreeCAD.Units.Quantity
import FreeCAD
import Draft
from sympy import Point3D, Line3D

class Inplane:
    def __init__(self, obj, label, node1, node2, referenceObject1, referenceObject2, reference1, reference2):   

     
        obj.addExtension("App::GroupExtensionPython")  
        
        #Create scripted object:
        obj.addProperty("App::PropertyString","label","In-plane joint","label",1).label = label
        obj.addProperty("App::PropertyString","joint","In-plane joint","joint",1).joint = 'in plane'
        obj.addProperty("App::PropertyString","node 1","In_plane joint","node 1",1).node_1 = node1.label
        obj.addProperty("App::PropertyString","node 2","In-plane joint","node 2",1).node_2 = node2.label
        
        #The absolute position is set at the node 1 position, only for animation, not for MBDyn sumulation:        
        obj.addProperty("App::PropertyDistance","absolute_pin_position_X","Absolute pin position","absolute_pin_position_X",1)
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Y","Absolute pin position","absolute_pin_position_Y",1)
        obj.addProperty("App::PropertyDistance","absolute_pin_position_Z","Absolute pin position","absolute_pin_position_Z",1)

        #The relative plane position is initially set to (0,0,0), this is, the line passes through node 1
        obj.addProperty("App::PropertyDistance","relative plane position X","Relative plane position","relative plane position X",1).relative_plane_position_X = 0.0
        obj.addProperty("App::PropertyDistance","relative plane position Y","Relative plane position","relative plane position Y",1).relative_plane_position_Y = 0.0
        obj.addProperty("App::PropertyDistance","relative plane position Z","Relative plane position","relative plane position Z",1).relative_plane_position_Z = 0.0
        
        #The relative plane orientation is initially set to (0,0,1), this is, the X-Y plane.
        obj.addProperty("App::PropertyString","relative normal direction","Relative normal direction","relative normal direction",1).relative_normal_direction = '0., 0., 1.'
        
        
        #The relative offset is initially set to (0,0,0), this is, node 2 is at the plane, without offset
        obj.addProperty("App::PropertyDistance","relative offset X","Node offset relative to plane","relative offset X",1).relative_offset_X = 0.0
        obj.addProperty("App::PropertyDistance","relative offset Y","Node offset relative to plane","relative offset Y",1).relative_offset_Y = 0.0
        obj.addProperty("App::PropertyDistance","relative offset Z","Node offset relative to plane","relative offset Z",1).relative_offset_Z = 0.0
        
        #The references define the position and orientation of the joint:                
        obj.addProperty("App::PropertyLinkSub","reference_1","Reference 1","reference_1")
        obj.addProperty("App::PropertyEnumeration","attachment_mode_1","Reference 1","attachment mode 1")
        obj.attachment_mode_1 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']
        
        obj.addProperty("App::PropertyLinkSub","reference_2","Reference 2","reference_2")        
        obj.addProperty("App::PropertyEnumeration","attachment_mode_2","Reference 2","attachment mode 2")
        obj.attachment_mode_2 = ['geometry´s center of mass', 'body´s center of mass', 'arc´s center']      

        if referenceObject1 == referenceObject2:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[1])            
        
        else:
            obj.reference_1 = (referenceObject1, reference1.SubElementNames[0])
            obj.reference_2 = (referenceObject2, reference2.SubElementNames[0]) 
        
        
        #Animation parameters:
        obj.addProperty("App::PropertyEnumeration","animate","Animation","animate")
        obj.animate=['false','true']

        obj.addProperty("App::PropertyEnumeration","frame","Animation","frame")
        obj.frame=['global','local']  
        
        obj.addProperty("App::PropertyString","structural dummy","Animation","structural dummy").structural_dummy = ''
        
        obj.addProperty("App::PropertyString","force vector multiplier","Animation","force vector multiplier").force_vector_multiplier = '1'

        obj.Proxy = self
                        

        #Add joint´s rotation axis. This axis determines the "absolute_pin_orientation_matrix" and the jont´s position:                    
        p1 = obj.reference_1[0].Shape.getElement(obj.reference_1[1][0]).CenterOfMass
        p2 = obj.reference_2[0].Shape.getElement(obj.reference_2[1][0]).CenterOfMass
        l = Draft.makeLine(p1, p2)
        l.Label = 'z: joint: '+ label
        l.ViewObject.ArrowType = u"Dot"            
        l.ViewObject.LineColor = (0.00,0.00,1.00)
        l.ViewObject.PointColor = (0.00,0.00,1.00)        
        l.ViewObject.DrawStyle = u"Dashed"       
        l.ViewObject.LineWidth = 1.00
        l.ViewObject.PointSize = 1.00           
        
        #Add the vector to visualize reaction forces        
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        #Add the vector to visualize reaction forces        
        p1 = FreeCAD.Vector((p1.x+p2.x)/2 , (p1.y+p2.y)/2, (p1.z+p2.z)/2)
        p2 = FreeCAD.Vector( p1.x+Llength.Value, p1.y+Llength.Value, p1.z+Llength.Value)         
        d = Draft.makeLine(p1, p2)
        d.ViewObject.LineColor = (1.00,0.00,0.00)
        d.ViewObject.PointColor = (1.00,0.00,0.00)  
        d.ViewObject.LineWidth = 1.00
        d.ViewObject.PointSize = 1.00
        d.ViewObject.EndArrow = True
        d.ViewObject.ArrowType = u"Arrow"
        d.ViewObject.ArrowSize = str(Llength/75)#+' mm'
        #d.ViewObject.Selectable = False
        d.Label = "jf: "+ label                    
                              
    def execute(self, fp):
    
        ##############################################################################Calculate the new orientation: 
        ZZ = FreeCAD.ActiveDocument.getObjectsByLabel("z: joint: "+fp.label)[0]#get the joint´s line    
        JF = FreeCAD.ActiveDocument.getObjectsByLabel("jf: "+fp.label)[0]
        Llength = FreeCAD.Units.Quantity(FreeCAD.ActiveDocument.getObjectsByLabel("X")[0].End[0]/4,FreeCAD.Units.Unit('mm'))
        #Two 3D points that define the joint´s line:
        if fp.attachment_mode_1 == 'geometry´s center of mass':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).CenterOfGravity
            
        if fp.attachment_mode_2 == 'geometry´s center of mass':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).CenterOfGravity
        
        if fp.attachment_mode_1 == 'body´s center of mass':
            p1 = fp.reference_1[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_2 == 'body´s center of mass':    
            p2 = fp.reference_2[0].Shape.CenterOfGravity
            
        if fp.attachment_mode_1 == 'arc´s center':
            p1 = fp.reference_1[0].Shape.getElement(fp.reference_1[1][0]).Curve.Center
            
        if fp.attachment_mode_2 == 'arc´s center':
            p2 = fp.reference_2[0].Shape.getElement(fp.reference_2[1][0]).Curve.Center
        
        ZZ.Start = p1
        ZZ.End = p2    
        
        p1, p2 = Point3D(ZZ.Start[0], ZZ.Start[1], ZZ.Start[2]), Point3D(ZZ.End[0], ZZ.End[1], ZZ.End[2]) 
        l1 = Line3D(p1, p2)#Line that defines the joint rotation axis
        #generate the orientation matrix:
        zzmagnitude = (l1.direction[0]**2+l1.direction[1]**2+l1.direction[2]**2)**0.5    
        fp.relative_normal_direction = str(l1.direction[0]/zzmagnitude) +", "+ str(l1.direction[1]/zzmagnitude) + ", " + str(l1.direction[2]/zzmagnitude)            
        
        #get and update the pin position:
        x = FreeCAD.Units.Quantity((ZZ.Start[0] + ZZ.End[0])/2,FreeCAD.Units.Unit('mm')) 
        y = FreeCAD.Units.Quantity((ZZ.Start[1] + ZZ.End[1])/2,FreeCAD.Units.Unit('mm')) 
        z = FreeCAD.Units.Quantity((ZZ.Start[2] + ZZ.End[2])/2,FreeCAD.Units.Unit('mm'))
              
        fp.absolute_pin_position_X = x
        fp.absolute_pin_position_Y = y
        fp.absolute_pin_position_Z = z
        
        JF.Start =  FreeCAD.Vector(x, y, z)
        JF.End = FreeCAD.Vector(x+Llength, y+Llength, z+Llength) 
        
        FreeCAD.Console.PrintMessage("IN-PLANE JOINT: " +fp.label+" successful recomputation...\n")                                  
                                  